import React, {useState} from 'react';
//Use to connect to the store
import { useDispatch, useSelector } from "react-redux";
import {formatTxt} from './actions';

export const App =(props) => {
  //Get information from the reducer
  let formatted_txt = useSelector(state => state.formatReducer.txt);
  //Create a local state
  const [title, setTitle] = useState(props.title);
  //Connect to the store
  const dispatch = useDispatch();

   function handleChangeTitle(e){
      const txt= e.target.value
      //modify the locate stat txt
      setTitle(txt);
      //call a reducer with associated action
      dispatch(formatTxt(txt));
    }

return (
    <div className="App">
      <h1> this is my first React Component</h1>
      <label htmlFor="titleInput">Title </label>
          <input type="text" id="titleInput" 
              onChange={handleChangeTitle} 
              value={title}
          />
          <h3>Original txt: {title}</h3>
          <h3>Formatted txt:{formatted_txt}</h3>
    </div>
  );
}


