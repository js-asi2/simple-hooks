import { combineReducers } from 'redux';
import formatReducer from './formatReducer';

/*
reducer that can contains set of reducer, usefull when several reducers are used at a timeS
*/
const globalReducer = combineReducers({
    formatReducer: formatReducer
});
export default globalReducer;